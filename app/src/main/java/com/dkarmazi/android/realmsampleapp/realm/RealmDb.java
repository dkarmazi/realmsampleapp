package com.dkarmazi.android.realmsampleapp.realm;

import android.content.Context;

import com.dkarmazi.android.realmsampleapp.realm.entities.TrackingEntry;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmQuery;

public class RealmDb {
    private static final String REALM_SCHEMA_NAME = "usertracking.realm";
    private static final String REALM_SCHEMA_TEST_NAME = "usertrackingtest.realm";
    private static final long REALM_SCHEMA_VERSION  = 0l;
    private static final String PRIMARY_KEY_FIELD_NAME = "id";

    public static void initTracking() {
        Realm realm = Realm.getDefaultInstance();

        if(getAllTrackingEntries(realm) == null) {
            List<TrackingEntry> trackingEntryList = RealmKeys.getTrackingEntryList();
            realm.beginTransaction();
            realm.insert(trackingEntryList);
            realm.commitTransaction();
        }
    }

    private static void updateTrackingEntry(final Realm realm,
                                            final boolean isAssync,
                                            final String key,
                                            final int counterDelta,
                                            final long timeLastUpdated,
                                            final long timeAccumulatedDelta,
                                            final UserTrackingUtility.TrackingEntryWriteListener trackingEntryWriteListener) {
        if(isAssync) {
            realm.executeTransactionAsync(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    queryAndUpdateTrackingEntry(realm, key, counterDelta, timeLastUpdated, timeAccumulatedDelta);
                }
            }, new Realm.Transaction.OnSuccess() {
                @Override
                public void onSuccess() {
                    if(trackingEntryWriteListener != null) {
                        // re-query
                        trackingEntryWriteListener.onTrackingEntryWritten(RealmDb.getTrackingEntryByKey(realm, key));
                    }
                }
            }, new Realm.Transaction.OnError() {
                @Override
                public void onError(Throwable error) {
                    if(trackingEntryWriteListener != null) {
                        trackingEntryWriteListener.onError();
                    }
                }
            });
        } else {
            realm.beginTransaction();
            queryAndUpdateTrackingEntry(realm, key, counterDelta, timeLastUpdated, timeAccumulatedDelta);
            realm.commitTransaction();
        }
    }

    // CAUTION: use only as follows:
    // 1. ASSYNC: inside realm.executeTransactionAsync()
    // 2. NOT ASSYNC: between realm.beginTransaction() and realm.commitTransaction()
    private static void queryAndUpdateTrackingEntry(Realm realm,
                                             String key,
                                             final int counterDelta,
                                             final long timeLastUpdated,
                                             final long timeAccumulatedDelta) {
        TrackingEntry trackingEntry = RealmDb.getTrackingEntryByKey(realm, key);
        if(trackingEntry != null) {
            TrackingEntry.update(trackingEntry, counterDelta, timeLastUpdated, timeAccumulatedDelta);
        }
    }

    public static void incrementCounterByKey(final Realm realm,
                                             boolean isAssync,
                                             final String key,
                                             final UserTrackingUtility.TrackingEntryWriteListener trackingEntryWriteListener) {
        updateTrackingEntry(realm, isAssync, key, 1, 0l, 0l, trackingEntryWriteListener);
    }

    public static void incrementCounterAndLastUpdated(Realm realm,
                                                      boolean isAssync,
                                                      final String key,
                                                      final UserTrackingUtility.TrackingEntryWriteListener trackingEntryWriteListener) {
        updateTrackingEntry(realm, isAssync, key, 1, getNow(), 0l, trackingEntryWriteListener);
    }

    public static void incrementAccumulatedTime(Realm realm,
                                                boolean isAssync,
                                                final String key,
                                                final long additionalTime,
                                                final UserTrackingUtility.TrackingEntryWriteListener trackingEntryWriteListener) {
        updateTrackingEntry(realm, isAssync, key, 0, 0l, additionalTime, trackingEntryWriteListener);
    }

    private static long getNow() {
        Calendar today = Calendar.getInstance(TimeZone.getTimeZone("America/New_York"));
        return today.getTimeInMillis();
    }

    public static TrackingEntry getTrackingEntryByKey(Realm realm, String key) {
        RealmQuery<TrackingEntry> trackingEntryRealmQuery = realm.where(TrackingEntry.class).equalTo(PRIMARY_KEY_FIELD_NAME, key);

        if (trackingEntryRealmQuery.count() > 0) {
            return trackingEntryRealmQuery.findFirst();
        } else {
            return null;
        }
    }

    public static List<TrackingEntry> getAllTrackingEntries(Realm realm) {
        RealmQuery<TrackingEntry> trackingEntryRealmQuery = realm.where(TrackingEntry.class);

        if(trackingEntryRealmQuery.count() > 0) {
            return trackingEntryRealmQuery.findAll();
        } else {
            return null;
        }
    }

    public static void initializeRealmInstance(Context context) {
        initializeRealmInstance(context, REALM_SCHEMA_NAME, REALM_SCHEMA_VERSION);
    }

    public static void initializeRealmInstanceTest(Context context) {
        initializeRealmInstance(context, REALM_SCHEMA_TEST_NAME, REALM_SCHEMA_VERSION);
    }

    private static void initializeRealmInstance(Context context,
                                               String realmSchemaName,
                                               long realmSchemaVersion) {
        // This instance initializes realm instance
        Realm.init(context);
        RealmConfiguration config = new RealmConfiguration.Builder()
                .name(realmSchemaName)
                .schemaVersion(realmSchemaVersion)
                .migration(new MyMigration())
                .build();

        Realm.setDefaultConfiguration(config);
    }

    public static class RealmKeys {
        // READING HABITS
        public static final String GALLERY_COUNT = "galleryCount";
        public static final String GALLERY_SWIPES_COUNT = "gallerySwipesCount";
        public static final String ARTICLE_COUNT = "articleCount";
        public static final String SESSIONS_COUNT = "sessionsCount";
        public static final String OVERALL_TIME_IN_APP = "overallTimeInApp";
        // PAGE SCROLL
        public static final String PAGE_SCROLL_UP_TO_10_PERCENT = "pageScrollUpTo10Percent";
        public static final String PAGE_SCROLL_UP_TO_20_PERCENT = "pageScrollUpTo20Percent";
        public static final String PAGE_SCROLL_UP_TO_30_PERCENT = "pageScrollUpTo30Percent";
        public static final String PAGE_SCROLL_UP_TO_40_PERCENT = "pageScrollUpTo40Percent";
        public static final String PAGE_SCROLL_UP_TO_50_PERCENT = "pageScrollUpTo50Percent";
        public static final String PAGE_SCROLL_UP_TO_60_PERCENT = "pageScrollUpTo60Percent";
        public static final String PAGE_SCROLL_UP_TO_70_PERCENT = "pageScrollUpTo70Percent";
        public static final String PAGE_SCROLL_UP_TO_80_PERCENT = "pageScrollUpTo80Percent";
        public static final String PAGE_SCROLL_UP_TO_90_PERCENT = "pageScrollUpTo90Percent";
        public static final String PAGE_SCROLL_UP_TO_100_PERCENT = "pageScrollUpTo100Percent";
        // SESSION TIME BUCKETS
        public static final String SESSION_TIME_UP_TO_10_SECONDS = "sessionTimeUpTo10Seconds";
        public static final String SESSION_TIME_UP_TO_30_SECONDS = "sessionTimeUpTo30Seconds";
        public static final String SESSION_TIME_UP_TO_60_SECONDS = "sessionTimeUpTo60Seconds";
        public static final String SESSION_TIME_UP_TO_180_SECONDS = "sessionTimeUpTo180Seconds";
        public static final String SESSION_TIME_UP_TO_600_SECONDS = "sessionTimeUpTo600Seconds";
        public static final String SESSION_TIME_INFINITY = "sessionTimeInfinity";
        // VIDEO COMPLETION
        public static final String VIDEO_COMPLETION_UP_TO_25_PERCENT = "videoCompletionUpTo25Percent";
        public static final String VIDEO_COMPLETION_UP_TO_50_PERCENT = "videoCompletionUpTo50Percent";
        public static final String VIDEO_COMPLETION_UP_TO_75_PERCENT = "videoCompletionUpTo75Percent";
        public static final String VIDEO_COMPLETION_UP_TO_100_PERCENT = "videoCompletionUpTo100Percent";
        // FEATURE HABITS
        public static final String OFFLINE_COUNT = "offlineCount";
        public static final String BOOKMARKING_COUNT = "bookmarkingCount";
        public static final String NIGHT_MODE_COUNT = "nightModeCount";
        public static final String SETTINGS_COUNT = "settingsCount";
        public static final String TEXT_SIZE_COUNT = "textSizeCount";
        public static final String LOCAL_MARKET_SELECTION_COUNT = "localMarketSelectionCount";
        public static final String BREAKING_NEWS_NOTIFICATIONS_COUNT = "breakingNewsNotificationsCount";
        public static final String NEWS_NOTIFICATIONS_COUNT = "newsNotificationsCount";
        public static final String SPORTS_NOTIFICATIONS_COUNT = "sportsNotificationsCount";
        public static final String LIFE_NOTIFICATIONS_COUNT = "lifeNotificationsCount";
        public static final String MONEY_NOTIFICATIONS_COUNT = "moneyNotificationsCount";
        public static final String TECH_NOTIFICATIONS_COUNT = "techNotificationsCount";

        public static List<TrackingEntry> getTrackingEntryList() {
            List<TrackingEntry> trackingEntryList = new ArrayList<>();

            trackingEntryList.add(new TrackingEntry(GALLERY_COUNT));
            trackingEntryList.add(new TrackingEntry(GALLERY_SWIPES_COUNT));
            trackingEntryList.add(new TrackingEntry(ARTICLE_COUNT));
            trackingEntryList.add(new TrackingEntry(SESSIONS_COUNT));
            trackingEntryList.add(new TrackingEntry(OVERALL_TIME_IN_APP));
            trackingEntryList.add(new TrackingEntry(PAGE_SCROLL_UP_TO_10_PERCENT));
            trackingEntryList.add(new TrackingEntry(PAGE_SCROLL_UP_TO_20_PERCENT));
            trackingEntryList.add(new TrackingEntry(PAGE_SCROLL_UP_TO_30_PERCENT));
            trackingEntryList.add(new TrackingEntry(PAGE_SCROLL_UP_TO_40_PERCENT));
            trackingEntryList.add(new TrackingEntry(PAGE_SCROLL_UP_TO_50_PERCENT));
            trackingEntryList.add(new TrackingEntry(PAGE_SCROLL_UP_TO_60_PERCENT));
            trackingEntryList.add(new TrackingEntry(PAGE_SCROLL_UP_TO_70_PERCENT));
            trackingEntryList.add(new TrackingEntry(PAGE_SCROLL_UP_TO_80_PERCENT));
            trackingEntryList.add(new TrackingEntry(PAGE_SCROLL_UP_TO_90_PERCENT));
            trackingEntryList.add(new TrackingEntry(PAGE_SCROLL_UP_TO_100_PERCENT));
            trackingEntryList.add(new TrackingEntry(SESSION_TIME_UP_TO_10_SECONDS));
            trackingEntryList.add(new TrackingEntry(SESSION_TIME_UP_TO_30_SECONDS));
            trackingEntryList.add(new TrackingEntry(SESSION_TIME_UP_TO_60_SECONDS));
            trackingEntryList.add(new TrackingEntry(SESSION_TIME_UP_TO_180_SECONDS));
            trackingEntryList.add(new TrackingEntry(SESSION_TIME_UP_TO_600_SECONDS));
            trackingEntryList.add(new TrackingEntry(SESSION_TIME_INFINITY));
            trackingEntryList.add(new TrackingEntry(VIDEO_COMPLETION_UP_TO_25_PERCENT));
            trackingEntryList.add(new TrackingEntry(VIDEO_COMPLETION_UP_TO_50_PERCENT));
            trackingEntryList.add(new TrackingEntry(VIDEO_COMPLETION_UP_TO_75_PERCENT));
            trackingEntryList.add(new TrackingEntry(VIDEO_COMPLETION_UP_TO_100_PERCENT));
            trackingEntryList.add(new TrackingEntry(OFFLINE_COUNT));
            trackingEntryList.add(new TrackingEntry(BOOKMARKING_COUNT));
            trackingEntryList.add(new TrackingEntry(NIGHT_MODE_COUNT));
            trackingEntryList.add(new TrackingEntry(SETTINGS_COUNT));
            trackingEntryList.add(new TrackingEntry(TEXT_SIZE_COUNT));
            trackingEntryList.add(new TrackingEntry(LOCAL_MARKET_SELECTION_COUNT));
            trackingEntryList.add(new TrackingEntry(BREAKING_NEWS_NOTIFICATIONS_COUNT));
            trackingEntryList.add(new TrackingEntry(NEWS_NOTIFICATIONS_COUNT));
            trackingEntryList.add(new TrackingEntry(SPORTS_NOTIFICATIONS_COUNT));
            trackingEntryList.add(new TrackingEntry(LIFE_NOTIFICATIONS_COUNT));
            trackingEntryList.add(new TrackingEntry(MONEY_NOTIFICATIONS_COUNT));
            trackingEntryList.add(new TrackingEntry(TECH_NOTIFICATIONS_COUNT));

            return trackingEntryList;
        }
    }
}
