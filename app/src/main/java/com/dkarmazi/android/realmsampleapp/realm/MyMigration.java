package com.dkarmazi.android.realmsampleapp.realm;

import android.util.Log;

import io.realm.DynamicRealm;
import io.realm.RealmMigration;


public class MyMigration implements RealmMigration {
    private static final String TAG = "dkarmazi-dkarmazi";

    @Override
    public void migrate(DynamicRealm realm, long oldVersion, long newVersion) {
        Log.d(TAG, "migrate() called with: realm = [" + realm + "], oldVersion = [" + oldVersion + "], newVersion = [" + newVersion + "]");
    }

    @Override
    public int hashCode() {
        return 1;
    }

    @Override
    public boolean equals(Object obj) {
        return true;
    }
}
