package com.dkarmazi.android.realmsampleapp.realm.entities;

import android.support.annotation.NonNull;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class TrackingEntry extends RealmObject {
    @PrimaryKey
    String id;
    int counter;
    long timeLastUpdated;
    long timeAccumulated;

    public TrackingEntry() {}

    public TrackingEntry(String id) {
        this.id = id;
        this.counter = 0;
        this.timeLastUpdated = 0l;
        this.timeAccumulated = 0l;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }

    public long getTimeLastUpdated() {
        return timeLastUpdated;
    }

    public void setTimeLastUpdated(long timeLastUpdated) {
        this.timeLastUpdated = timeLastUpdated;
    }

    public long getTimeAccumulated() {
        return timeAccumulated;
    }

    public void setTimeAccumulated(long timeAccumulated) {
        this.timeAccumulated = timeAccumulated;
    }

    public static TrackingEntry update(@NonNull TrackingEntry trackingEntry,
                                       int counterDelta,
                                       long timeLastUpdated,
                                       long timeAccumulatedDelta) {
        final int newCounter = trackingEntry.getCounter() + counterDelta;
        final long newTimeAccumulated = trackingEntry.getTimeAccumulated() + timeAccumulatedDelta;

        trackingEntry.setCounter(newCounter);
        trackingEntry.setTimeLastUpdated(timeLastUpdated);
        trackingEntry.setTimeAccumulated(newTimeAccumulated);

        return trackingEntry;
    }

    public static TrackingEntry clone(@NonNull TrackingEntry trackingEntry) {
        TrackingEntry newTrackingEntry = new TrackingEntry();

        newTrackingEntry.id = trackingEntry.getId();
        newTrackingEntry.counter = trackingEntry.getCounter();
        newTrackingEntry.timeLastUpdated = trackingEntry.getTimeLastUpdated();
        newTrackingEntry.timeAccumulated = trackingEntry.getTimeAccumulated();

        return newTrackingEntry;
    }
}
