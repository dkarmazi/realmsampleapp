package com.dkarmazi.android.realmsampleapp.realm;

import io.realm.Realm;

public class UserTrackingUtilityImpl implements UserTrackingUtility {
    @Override
    public void trackGalleryCount(Realm realm, boolean isAssync, TrackingEntryWriteListener trackingEntryWriteListener) {
        RealmDb.incrementCounterByKey(realm, isAssync, RealmDb.RealmKeys.GALLERY_COUNT, trackingEntryWriteListener);
    }

    @Override
    public void trackGallerySwipesCount(Realm realm, boolean isAssync, TrackingEntryWriteListener trackingEntryWriteListener) {
        RealmDb.incrementCounterByKey(realm, isAssync, RealmDb.RealmKeys.GALLERY_SWIPES_COUNT, trackingEntryWriteListener);
    }

    @Override
    public void trackArticleCount(Realm realm, boolean isAssync, TrackingEntryWriteListener trackingEntryWriteListener) {
        RealmDb.incrementCounterByKey(realm, isAssync, RealmDb.RealmKeys.ARTICLE_COUNT, trackingEntryWriteListener);
    }

    @Override
    public void trackSessionCount(Realm realm, boolean isAssync, TrackingEntryWriteListener trackingEntryWriteListener) {
        RealmDb.incrementCounterByKey(realm, isAssync, RealmDb.RealmKeys.SESSIONS_COUNT, trackingEntryWriteListener);
    }

    @Override
    public void trackOverallTimeInApp(Realm realm, boolean isAssync, long spendTime, TrackingEntryWriteListener trackingEntryWriteListener) {
        RealmDb.incrementAccumulatedTime(realm, isAssync, RealmDb.RealmKeys.OVERALL_TIME_IN_APP, spendTime, trackingEntryWriteListener);
    }

    @Override
    public void trackScrollUpTp10Percent(Realm realm, boolean isAssync, TrackingEntryWriteListener trackingEntryWriteListener) {
        RealmDb.incrementCounterByKey(realm, isAssync, RealmDb.RealmKeys.PAGE_SCROLL_UP_TO_10_PERCENT, trackingEntryWriteListener);
    }

    @Override
    public void trackScrollUpTp20Percent(Realm realm, boolean isAssync, TrackingEntryWriteListener trackingEntryWriteListener) {
        RealmDb.incrementCounterByKey(realm, isAssync, RealmDb.RealmKeys.PAGE_SCROLL_UP_TO_20_PERCENT, trackingEntryWriteListener);
    }

    @Override
    public void trackScrollUpTp30Percent(Realm realm, boolean isAssync, TrackingEntryWriteListener trackingEntryWriteListener) {
        RealmDb.incrementCounterByKey(realm, isAssync, RealmDb.RealmKeys.PAGE_SCROLL_UP_TO_30_PERCENT, trackingEntryWriteListener);
    }

    @Override
    public void trackScrollUpTp40Percent(Realm realm, boolean isAssync, TrackingEntryWriteListener trackingEntryWriteListener) {
        RealmDb.incrementCounterByKey(realm, isAssync, RealmDb.RealmKeys.PAGE_SCROLL_UP_TO_40_PERCENT, trackingEntryWriteListener);
    }

    @Override
    public void trackScrollUpTp50Percent(Realm realm, boolean isAssync, TrackingEntryWriteListener trackingEntryWriteListener) {
        RealmDb.incrementCounterByKey(realm, isAssync, RealmDb.RealmKeys.PAGE_SCROLL_UP_TO_50_PERCENT, trackingEntryWriteListener);
    }

    @Override
    public void trackScrollUpTp60Percent(Realm realm, boolean isAssync, TrackingEntryWriteListener trackingEntryWriteListener) {
        RealmDb.incrementCounterByKey(realm, isAssync, RealmDb.RealmKeys.PAGE_SCROLL_UP_TO_60_PERCENT, trackingEntryWriteListener);
    }

    @Override
    public void trackScrollUpTp70Percent(Realm realm, boolean isAssync, TrackingEntryWriteListener trackingEntryWriteListener) {
        RealmDb.incrementCounterByKey(realm, isAssync, RealmDb.RealmKeys.PAGE_SCROLL_UP_TO_70_PERCENT, trackingEntryWriteListener);
    }

    @Override
    public void trackScrollUpTp80Percent(Realm realm, boolean isAssync, TrackingEntryWriteListener trackingEntryWriteListener) {
        RealmDb.incrementCounterByKey(realm, isAssync, RealmDb.RealmKeys.PAGE_SCROLL_UP_TO_80_PERCENT, trackingEntryWriteListener);
    }

    @Override
    public void trackScrollUpTp90Percent(Realm realm, boolean isAssync, TrackingEntryWriteListener trackingEntryWriteListener) {
        RealmDb.incrementCounterByKey(realm, isAssync, RealmDb.RealmKeys.PAGE_SCROLL_UP_TO_90_PERCENT, trackingEntryWriteListener);
    }

    @Override
    public void trackScrollUpTp100Percent(Realm realm, boolean isAssync, TrackingEntryWriteListener trackingEntryWriteListener) {
        RealmDb.incrementCounterByKey(realm, isAssync, RealmDb.RealmKeys.PAGE_SCROLL_UP_TO_100_PERCENT, trackingEntryWriteListener);
    }

    @Override
    public void trackSessionTimeUpTo10Seconds(Realm realm, boolean isAssync, TrackingEntryWriteListener trackingEntryWriteListener) {
        RealmDb.incrementCounterByKey(realm, isAssync, RealmDb.RealmKeys.SESSION_TIME_UP_TO_10_SECONDS, trackingEntryWriteListener);
    }

    @Override
    public void trackSessionTimeUpTo30Seconds(Realm realm, boolean isAssync, TrackingEntryWriteListener trackingEntryWriteListener) {
        RealmDb.incrementCounterByKey(realm, isAssync, RealmDb.RealmKeys.SESSION_TIME_UP_TO_30_SECONDS, trackingEntryWriteListener);
    }

    @Override
    public void trackSessionTimeUpTo60Seconds(Realm realm, boolean isAssync, TrackingEntryWriteListener trackingEntryWriteListener) {
        RealmDb.incrementCounterByKey(realm, isAssync, RealmDb.RealmKeys.SESSION_TIME_UP_TO_60_SECONDS, trackingEntryWriteListener);
    }

    @Override
    public void trackSessionTimeUpTo180Seconds(Realm realm, boolean isAssync, TrackingEntryWriteListener trackingEntryWriteListener) {
        RealmDb.incrementCounterByKey(realm, isAssync, RealmDb.RealmKeys.SESSION_TIME_UP_TO_180_SECONDS, trackingEntryWriteListener);
    }

    @Override
    public void trackSessionTimeUpTo600Seconds(Realm realm, boolean isAssync, TrackingEntryWriteListener trackingEntryWriteListener) {
        RealmDb.incrementCounterByKey(realm, isAssync, RealmDb.RealmKeys.SESSION_TIME_UP_TO_600_SECONDS, trackingEntryWriteListener);
    }

    @Override
    public void trackSessionTimeUpToInfinity(Realm realm, boolean isAssync, TrackingEntryWriteListener trackingEntryWriteListener) {
        RealmDb.incrementCounterByKey(realm, isAssync, RealmDb.RealmKeys.SESSION_TIME_INFINITY, trackingEntryWriteListener);
    }

    @Override
    public void trackVideoCompletionUpTo25Percent(Realm realm, boolean isAssync, TrackingEntryWriteListener trackingEntryWriteListener) {
        RealmDb.incrementCounterByKey(realm, isAssync, RealmDb.RealmKeys.VIDEO_COMPLETION_UP_TO_25_PERCENT, trackingEntryWriteListener);
    }

    @Override
    public void trackVideoCompletionUpTo50Percent(Realm realm, boolean isAssync, TrackingEntryWriteListener trackingEntryWriteListener) {
        RealmDb.incrementCounterByKey(realm, isAssync, RealmDb.RealmKeys.VIDEO_COMPLETION_UP_TO_50_PERCENT, trackingEntryWriteListener);
    }

    @Override
    public void trackVideoCompletionUpTo75Percent(Realm realm, boolean isAssync, TrackingEntryWriteListener trackingEntryWriteListener) {
        RealmDb.incrementCounterByKey(realm, isAssync, RealmDb.RealmKeys.VIDEO_COMPLETION_UP_TO_75_PERCENT, trackingEntryWriteListener);
    }

    @Override
    public void trackVideoCompletionUpTo100Percent(Realm realm, boolean isAssync, TrackingEntryWriteListener trackingEntryWriteListener) {
        RealmDb.incrementCounterByKey(realm, isAssync, RealmDb.RealmKeys.VIDEO_COMPLETION_UP_TO_100_PERCENT, trackingEntryWriteListener);
    }

    @Override
    public void trackOfflineCount(Realm realm, boolean isAssync, TrackingEntryWriteListener trackingEntryWriteListener) {
        RealmDb.incrementCounterAndLastUpdated(realm, isAssync, RealmDb.RealmKeys.OFFLINE_COUNT, trackingEntryWriteListener);
    }

    @Override
    public void trackBookmarkingCount(Realm realm, boolean isAssync, TrackingEntryWriteListener trackingEntryWriteListener) {
        RealmDb.incrementCounterAndLastUpdated(realm, isAssync, RealmDb.RealmKeys.BOOKMARKING_COUNT, trackingEntryWriteListener);
    }

    @Override
    public void trackNightModeCount(Realm realm, boolean isAssync, TrackingEntryWriteListener trackingEntryWriteListener) {
        RealmDb.incrementCounterAndLastUpdated(realm, isAssync, RealmDb.RealmKeys.NIGHT_MODE_COUNT, trackingEntryWriteListener);
    }

    @Override
    public void trackSettingsCount(Realm realm, boolean isAssync, TrackingEntryWriteListener trackingEntryWriteListener) {
        RealmDb.incrementCounterAndLastUpdated(realm, isAssync, RealmDb.RealmKeys.SETTINGS_COUNT, trackingEntryWriteListener);
    }

    @Override
    public void trackTextSize(Realm realm, boolean isAssync, TrackingEntryWriteListener trackingEntryWriteListener) {
        RealmDb.incrementCounterAndLastUpdated(realm, isAssync, RealmDb.RealmKeys.TEXT_SIZE_COUNT, trackingEntryWriteListener);
    }

    @Override
    public void trackLocalMarketSelectionCount(Realm realm, boolean isAssync, TrackingEntryWriteListener trackingEntryWriteListener) {
        RealmDb.incrementCounterAndLastUpdated(realm, isAssync, RealmDb.RealmKeys.LOCAL_MARKET_SELECTION_COUNT, trackingEntryWriteListener);
    }

    @Override
    public void trackBreakingNewsNotificationsCount(Realm realm, boolean isAssync, TrackingEntryWriteListener trackingEntryWriteListener) {
        RealmDb.incrementCounterAndLastUpdated(realm, isAssync, RealmDb.RealmKeys.BREAKING_NEWS_NOTIFICATIONS_COUNT, trackingEntryWriteListener);
    }

    @Override
    public void trackNewsNotificationsCount(Realm realm, boolean isAssync, TrackingEntryWriteListener trackingEntryWriteListener) {
        RealmDb.incrementCounterAndLastUpdated(realm, isAssync, RealmDb.RealmKeys.NEWS_NOTIFICATIONS_COUNT, trackingEntryWriteListener);
    }

    @Override
    public void trackSportsNotificationsCount(Realm realm, boolean isAssync, TrackingEntryWriteListener trackingEntryWriteListener) {
        RealmDb.incrementCounterAndLastUpdated(realm, isAssync, RealmDb.RealmKeys.SPORTS_NOTIFICATIONS_COUNT, trackingEntryWriteListener);
    }

    @Override
    public void trackLifeNotificationsCount(Realm realm, boolean isAssync, TrackingEntryWriteListener trackingEntryWriteListener) {
        RealmDb.incrementCounterAndLastUpdated(realm, isAssync, RealmDb.RealmKeys.LIFE_NOTIFICATIONS_COUNT, trackingEntryWriteListener);
    }

    @Override
    public void trackMoneyNotificationsCount(Realm realm, boolean isAssync, TrackingEntryWriteListener trackingEntryWriteListener) {
        RealmDb.incrementCounterAndLastUpdated(realm, isAssync, RealmDb.RealmKeys.MONEY_NOTIFICATIONS_COUNT, trackingEntryWriteListener);
    }

    @Override
    public void trackTechNotificationsCount(Realm realm, boolean isAssync, TrackingEntryWriteListener trackingEntryWriteListener) {
        RealmDb.incrementCounterAndLastUpdated(realm, isAssync, RealmDb.RealmKeys.TECH_NOTIFICATIONS_COUNT, trackingEntryWriteListener);
    }
}
