package com.dkarmazi.android.realmsampleapp.realm;

import android.support.annotation.Nullable;

import com.dkarmazi.android.realmsampleapp.realm.entities.TrackingEntry;

import io.realm.Realm;

public interface UserTrackingUtility {
    interface TrackingEntryWriteListener {
        void onTrackingEntryWritten(TrackingEntry newTrackingEntry);
        void onError();
    }

    void trackGalleryCount(Realm realm, boolean isAssync, @Nullable TrackingEntryWriteListener trackingEntryWriteListener);
    void trackGallerySwipesCount(Realm realm, boolean isAssync, @Nullable TrackingEntryWriteListener trackingEntryWriteListener);
    void trackArticleCount(Realm realm, boolean isAssync, @Nullable TrackingEntryWriteListener trackingEntryWriteListener);
    void trackSessionCount(Realm realm, boolean isAssync, @Nullable TrackingEntryWriteListener trackingEntryWriteListener);
    void trackOverallTimeInApp(Realm realm, boolean isAssync, long spendTime, @Nullable TrackingEntryWriteListener longWriteListener);

    void trackScrollUpTp10Percent(Realm realm, boolean isAssync, @Nullable TrackingEntryWriteListener trackingEntryWriteListener);
    void trackScrollUpTp20Percent(Realm realm, boolean isAssync, @Nullable TrackingEntryWriteListener trackingEntryWriteListener);
    void trackScrollUpTp30Percent(Realm realm, boolean isAssync, @Nullable TrackingEntryWriteListener trackingEntryWriteListener);
    void trackScrollUpTp40Percent(Realm realm, boolean isAssync, @Nullable TrackingEntryWriteListener trackingEntryWriteListener);
    void trackScrollUpTp50Percent(Realm realm, boolean isAssync, @Nullable TrackingEntryWriteListener trackingEntryWriteListener);
    void trackScrollUpTp60Percent(Realm realm, boolean isAssync, @Nullable TrackingEntryWriteListener trackingEntryWriteListener);
    void trackScrollUpTp70Percent(Realm realm, boolean isAssync, @Nullable TrackingEntryWriteListener trackingEntryWriteListener);
    void trackScrollUpTp80Percent(Realm realm, boolean isAssync, @Nullable TrackingEntryWriteListener trackingEntryWriteListener);
    void trackScrollUpTp90Percent(Realm realm, boolean isAssync, @Nullable TrackingEntryWriteListener trackingEntryWriteListener);
    void trackScrollUpTp100Percent(Realm realm, boolean isAssync, @Nullable TrackingEntryWriteListener trackingEntryWriteListener);

    void trackSessionTimeUpTo10Seconds(Realm realm, boolean isAssync, @Nullable TrackingEntryWriteListener trackingEntryWriteListener);
    void trackSessionTimeUpTo30Seconds(Realm realm, boolean isAssync, @Nullable TrackingEntryWriteListener trackingEntryWriteListener);
    void trackSessionTimeUpTo60Seconds(Realm realm, boolean isAssync, @Nullable TrackingEntryWriteListener trackingEntryWriteListener);
    void trackSessionTimeUpTo180Seconds(Realm realm, boolean isAssync, @Nullable TrackingEntryWriteListener trackingEntryWriteListener);
    void trackSessionTimeUpTo600Seconds(Realm realm, boolean isAssync, @Nullable TrackingEntryWriteListener trackingEntryWriteListener);
    void trackSessionTimeUpToInfinity(Realm realm, boolean isAssync, @Nullable TrackingEntryWriteListener trackingEntryWriteListener);

    void trackVideoCompletionUpTo25Percent(Realm realm, boolean isAssync, @Nullable TrackingEntryWriteListener trackingEntryWriteListener);
    void trackVideoCompletionUpTo50Percent(Realm realm, boolean isAssync, @Nullable TrackingEntryWriteListener trackingEntryWriteListener);
    void trackVideoCompletionUpTo75Percent(Realm realm, boolean isAssync, @Nullable TrackingEntryWriteListener trackingEntryWriteListener);
    void trackVideoCompletionUpTo100Percent(Realm realm, boolean isAssync, @Nullable TrackingEntryWriteListener trackingEntryWriteListener);

    void trackOfflineCount(Realm realm, boolean isAssync, @Nullable TrackingEntryWriteListener trackingEntryWriteListener);
    void trackBookmarkingCount(Realm realm, boolean isAssync, @Nullable TrackingEntryWriteListener trackingEntryWriteListener);
    void trackNightModeCount(Realm realm, boolean isAssync, @Nullable TrackingEntryWriteListener trackingEntryWriteListener);
    void trackSettingsCount(Realm realm, boolean isAssync, @Nullable TrackingEntryWriteListener trackingEntryWriteListener);

    void trackTextSize(Realm realm, boolean isAssync, @Nullable TrackingEntryWriteListener trackingEntryWriteListener);
    void trackLocalMarketSelectionCount(Realm realm, boolean isAssync, @Nullable TrackingEntryWriteListener trackingEntryWriteListener);
    void trackBreakingNewsNotificationsCount(Realm realm, boolean isAssync, @Nullable TrackingEntryWriteListener trackingEntryWriteListener);
    void trackNewsNotificationsCount(Realm realm, boolean isAssync, @Nullable TrackingEntryWriteListener trackingEntryWriteListener);
    void trackSportsNotificationsCount(Realm realm, boolean isAssync, @Nullable TrackingEntryWriteListener trackingEntryWriteListener);
    void trackLifeNotificationsCount(Realm realm, boolean isAssync, @Nullable TrackingEntryWriteListener trackingEntryWriteListener);
    void trackMoneyNotificationsCount(Realm realm, boolean isAssync, @Nullable TrackingEntryWriteListener trackingEntryWriteListener);
    void trackTechNotificationsCount(Realm realm, boolean isAssync, @Nullable TrackingEntryWriteListener trackingEntryWriteListener);
}
