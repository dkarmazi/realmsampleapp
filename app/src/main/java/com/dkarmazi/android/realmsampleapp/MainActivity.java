package com.dkarmazi.android.realmsampleapp;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.dkarmazi.android.realmsampleapp.databinding.ActivityMainBinding;
import com.dkarmazi.android.realmsampleapp.entities.DataBinding;
import com.dkarmazi.android.realmsampleapp.realm.UserTrackingUtility;
import com.dkarmazi.android.realmsampleapp.realm.entities.TrackingEntry;

import io.realm.Realm;

public class MainActivity extends AppCompatActivity {
    private Realm realm;
    private UserTrackingUtility userTrackingUtility;

    public interface InteractionHandler {
        void onButtonClicked();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.realm = Realm.getDefaultInstance();
        this.userTrackingUtility = ((MyApplication) getApplication()).getUserTrackingUtility();

        final ActivityMainBinding activityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        final DataBinding dataBinding = new DataBinding();
        dataBinding.counter = "0";

        activityMainBinding.setDatabinding(dataBinding);
        activityMainBinding.setInteractionhandler(new InteractionHandler() {
            @Override
            public void onButtonClicked() {
                userTrackingUtility.trackGalleryCount(realm, true, new UserTrackingUtility.TrackingEntryWriteListener() {
                    @Override
                    public void onTrackingEntryWritten(TrackingEntry newTrackingEntry) {
                        dataBinding.counter = "" + newTrackingEntry.getCounter();
                        activityMainBinding.setDatabinding(dataBinding);
                    }

                    @Override
                    public void onError() {

                    }
                });
            }
        });
    }

    @Override
    protected void onStop() {
        realm.close();

        super.onStop();
    }
}
