package com.dkarmazi.android.realmsampleapp;

import android.app.Application;

import com.dkarmazi.android.realmsampleapp.realm.RealmDb;
import com.dkarmazi.android.realmsampleapp.realm.UserTrackingUtility;
import com.dkarmazi.android.realmsampleapp.realm.UserTrackingUtilityImpl;

public class MyApplication extends Application {
    private UserTrackingUtility userTrackingUtility;

    @Override
    public void onCreate() {
        super.onCreate();
        RealmDb.initializeRealmInstance(this);
        RealmDb.initTracking();
    }

    public UserTrackingUtility getUserTrackingUtility() {
        if(userTrackingUtility == null) {
            userTrackingUtility = new UserTrackingUtilityImpl();
        }

        return userTrackingUtility;
    }
}
