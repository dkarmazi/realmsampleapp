package com.dkarmazi.android.realmsampleapp;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.dkarmazi.android.realmsampleapp.realm.RealmDb;
import com.dkarmazi.android.realmsampleapp.realm.UserTrackingUtility;
import com.dkarmazi.android.realmsampleapp.realm.UserTrackingUtilityImpl;
import com.dkarmazi.android.realmsampleapp.realm.entities.TrackingEntry;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import io.realm.Realm;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {
    private static Realm realm;
    private static UserTrackingUtility userTrackingUtility;

    @BeforeClass
    public static void initialize() {
        Context appContext = InstrumentationRegistry.getTargetContext();
        RealmDb.initializeRealmInstanceTest(appContext);
        RealmDb.initTracking();
        realm = Realm.getDefaultInstance();
        userTrackingUtility = new UserTrackingUtilityImpl();
    }

    @Test
    public void useAppContext() throws Exception {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();

        assertEquals("com.dkarmazi.android.realmsampleapp", appContext.getPackageName());
    }

    private void compareCounterDelta(TrackingEntry trackingEntry, TrackingEntry oldTrackingEntry) {
        assertEquals(oldTrackingEntry.getId(), trackingEntry.getId());
        assertEquals(oldTrackingEntry.getCounter(), trackingEntry.getCounter() - 1);
        assertEquals(oldTrackingEntry.getTimeLastUpdated(), trackingEntry.getTimeLastUpdated());
        assertEquals(oldTrackingEntry.getTimeAccumulated(), trackingEntry.getTimeAccumulated());
    }

    private void compareCounterAndLastUpdatedDeltas(TrackingEntry trackingEntry, TrackingEntry oldTrackingEntry) {
        assertEquals(oldTrackingEntry.getId(), trackingEntry.getId());
        assertEquals(oldTrackingEntry.getCounter(), trackingEntry.getCounter() - 1);
        assertTrue(oldTrackingEntry.getTimeLastUpdated() < trackingEntry.getTimeLastUpdated());
        assertEquals(oldTrackingEntry.getTimeAccumulated(), trackingEntry.getTimeAccumulated());
    }

    private void compareTimeAccumulatedDelta(TrackingEntry trackingEntry, TrackingEntry oldTrackingEntry, long delta) {
        assertEquals(oldTrackingEntry.getId(), trackingEntry.getId());
        assertEquals(oldTrackingEntry.getCounter(), trackingEntry.getCounter());
        assertEquals(oldTrackingEntry.getTimeLastUpdated(), trackingEntry.getTimeLastUpdated());
        assertEquals(oldTrackingEntry.getTimeAccumulated(), trackingEntry.getTimeAccumulated() - delta);
    }

    @Test
    public void trackGalleryCount() throws Exception {
        TrackingEntry trackingEntry = RealmDb.getTrackingEntryByKey(realm, RealmDb.RealmKeys.GALLERY_COUNT);
        TrackingEntry oldTrackingEntry = TrackingEntry.clone(trackingEntry);
        userTrackingUtility.trackGalleryCount(realm, false, null);
        compareCounterDelta(trackingEntry, oldTrackingEntry);
    }

    @Test
    public void trackGallerySwipesCount() throws Exception {
        TrackingEntry trackingEntry = RealmDb.getTrackingEntryByKey(realm, RealmDb.RealmKeys.GALLERY_SWIPES_COUNT);
        TrackingEntry oldTrackingEntry = TrackingEntry.clone(trackingEntry);
        userTrackingUtility.trackGallerySwipesCount(realm, false, null);
        compareCounterDelta(trackingEntry, oldTrackingEntry);
    }

    @Test
    public void trackArticleCount() throws Exception {
        TrackingEntry trackingEntry = RealmDb.getTrackingEntryByKey(realm, RealmDb.RealmKeys.ARTICLE_COUNT);
        TrackingEntry oldTrackingEntry = TrackingEntry.clone(trackingEntry);
        userTrackingUtility.trackArticleCount(realm, false, null);
        compareCounterDelta(trackingEntry, oldTrackingEntry);
    }

    @Test
    public void trackSessionCount() throws Exception {
        TrackingEntry trackingEntry = RealmDb.getTrackingEntryByKey(realm, RealmDb.RealmKeys.SESSIONS_COUNT);
        TrackingEntry oldTrackingEntry = TrackingEntry.clone(trackingEntry);
        userTrackingUtility.trackSessionCount(realm, false, null);
        compareCounterDelta(trackingEntry, oldTrackingEntry);
    }

    @Test
    public void trackOverallTimeInApp() throws Exception {
        TrackingEntry trackingEntry = RealmDb.getTrackingEntryByKey(realm, RealmDb.RealmKeys.OVERALL_TIME_IN_APP);
        TrackingEntry oldTrackingEntry = TrackingEntry.clone(trackingEntry);
        long delta = 123l;
        userTrackingUtility.trackOverallTimeInApp(realm, false, delta, null);
        compareTimeAccumulatedDelta(trackingEntry, oldTrackingEntry, delta);
    }

    @Test
    public void trackScrollUpTp10Percent() throws Exception {
        TrackingEntry trackingEntry = RealmDb.getTrackingEntryByKey(realm, RealmDb.RealmKeys.PAGE_SCROLL_UP_TO_10_PERCENT);
        TrackingEntry oldTrackingEntry = TrackingEntry.clone(trackingEntry);
        userTrackingUtility.trackScrollUpTp10Percent(realm, false, null);
        compareCounterDelta(trackingEntry, oldTrackingEntry);
    }

    @Test
    public void trackScrollUpTp20Percent() throws Exception {
        TrackingEntry trackingEntry = RealmDb.getTrackingEntryByKey(realm, RealmDb.RealmKeys.PAGE_SCROLL_UP_TO_20_PERCENT);
        TrackingEntry oldTrackingEntry = TrackingEntry.clone(trackingEntry);
        userTrackingUtility.trackScrollUpTp20Percent(realm, false, null);
        compareCounterDelta(trackingEntry, oldTrackingEntry);
    }

    @Test
    public void trackScrollUpTp30Percent() throws Exception {
        TrackingEntry trackingEntry = RealmDb.getTrackingEntryByKey(realm, RealmDb.RealmKeys.PAGE_SCROLL_UP_TO_30_PERCENT);
        TrackingEntry oldTrackingEntry = TrackingEntry.clone(trackingEntry);
        userTrackingUtility.trackScrollUpTp30Percent(realm, false, null);
        compareCounterDelta(trackingEntry, oldTrackingEntry);
    }

    @Test
    public void trackScrollUpTp40Percent() throws Exception {
        TrackingEntry trackingEntry = RealmDb.getTrackingEntryByKey(realm, RealmDb.RealmKeys.PAGE_SCROLL_UP_TO_40_PERCENT);
        TrackingEntry oldTrackingEntry = TrackingEntry.clone(trackingEntry);
        userTrackingUtility.trackScrollUpTp40Percent(realm, false, null);
        compareCounterDelta(trackingEntry, oldTrackingEntry);
    }

    @Test
    public void trackScrollUpTp50Percent() throws Exception {
        TrackingEntry trackingEntry = RealmDb.getTrackingEntryByKey(realm, RealmDb.RealmKeys.PAGE_SCROLL_UP_TO_50_PERCENT);
        TrackingEntry oldTrackingEntry = TrackingEntry.clone(trackingEntry);
        userTrackingUtility.trackScrollUpTp50Percent(realm, false, null);
        compareCounterDelta(trackingEntry, oldTrackingEntry);
    }

    @Test
    public void trackScrollUpTp60Percent() throws Exception {
        TrackingEntry trackingEntry = RealmDb.getTrackingEntryByKey(realm, RealmDb.RealmKeys.PAGE_SCROLL_UP_TO_60_PERCENT);
        TrackingEntry oldTrackingEntry = TrackingEntry.clone(trackingEntry);
        userTrackingUtility.trackScrollUpTp60Percent(realm, false, null);
        compareCounterDelta(trackingEntry, oldTrackingEntry);
    }

    @Test
    public void trackScrollUpTp70Percent() throws Exception {
        TrackingEntry trackingEntry = RealmDb.getTrackingEntryByKey(realm, RealmDb.RealmKeys.PAGE_SCROLL_UP_TO_70_PERCENT);
        TrackingEntry oldTrackingEntry = TrackingEntry.clone(trackingEntry);
        userTrackingUtility.trackScrollUpTp70Percent(realm, false, null);
        compareCounterDelta(trackingEntry, oldTrackingEntry);
    }

    @Test
    public void trackScrollUpTp80Percent() throws Exception {
        TrackingEntry trackingEntry = RealmDb.getTrackingEntryByKey(realm, RealmDb.RealmKeys.PAGE_SCROLL_UP_TO_80_PERCENT);
        TrackingEntry oldTrackingEntry = TrackingEntry.clone(trackingEntry);
        userTrackingUtility.trackScrollUpTp80Percent(realm, false, null);
        compareCounterDelta(trackingEntry, oldTrackingEntry);
    }

    @Test
    public void trackScrollUpTp90Percent() throws Exception {
        TrackingEntry trackingEntry = RealmDb.getTrackingEntryByKey(realm, RealmDb.RealmKeys.PAGE_SCROLL_UP_TO_90_PERCENT);
        TrackingEntry oldTrackingEntry = TrackingEntry.clone(trackingEntry);
        userTrackingUtility.trackScrollUpTp90Percent(realm, false, null);
        compareCounterDelta(trackingEntry, oldTrackingEntry);
    }

    @Test
    public void trackScrollUpTp100Percent() throws Exception {
        TrackingEntry trackingEntry = RealmDb.getTrackingEntryByKey(realm, RealmDb.RealmKeys.PAGE_SCROLL_UP_TO_100_PERCENT);
        TrackingEntry oldTrackingEntry = TrackingEntry.clone(trackingEntry);
        userTrackingUtility.trackScrollUpTp100Percent(realm, false, null);
        compareCounterDelta(trackingEntry, oldTrackingEntry);
    }

    @Test
    public void trackSessionTimeUpTo10Seconds() throws Exception {
        TrackingEntry trackingEntry = RealmDb.getTrackingEntryByKey(realm, RealmDb.RealmKeys.SESSION_TIME_UP_TO_10_SECONDS);
        TrackingEntry oldTrackingEntry = TrackingEntry.clone(trackingEntry);
        userTrackingUtility.trackSessionTimeUpTo10Seconds(realm, false, null);
        compareCounterDelta(trackingEntry, oldTrackingEntry);
    }

    @Test
    public void trackSessionTimeUpTo30Seconds() throws Exception {
        TrackingEntry trackingEntry = RealmDb.getTrackingEntryByKey(realm, RealmDb.RealmKeys.SESSION_TIME_UP_TO_30_SECONDS);
        TrackingEntry oldTrackingEntry = TrackingEntry.clone(trackingEntry);
        userTrackingUtility.trackSessionTimeUpTo30Seconds(realm, false, null);
        compareCounterDelta(trackingEntry, oldTrackingEntry);
    }

    @Test
    public void trackSessionTimeUpTo60Seconds() throws Exception {
        TrackingEntry trackingEntry = RealmDb.getTrackingEntryByKey(realm, RealmDb.RealmKeys.SESSION_TIME_UP_TO_60_SECONDS);
        TrackingEntry oldTrackingEntry = TrackingEntry.clone(trackingEntry);
        userTrackingUtility.trackSessionTimeUpTo60Seconds(realm, false, null);
        compareCounterDelta(trackingEntry, oldTrackingEntry);
    }

    @Test
    public void trackSessionTimeUpTo180Seconds() throws Exception {
        TrackingEntry trackingEntry = RealmDb.getTrackingEntryByKey(realm, RealmDb.RealmKeys.SESSION_TIME_UP_TO_180_SECONDS);
        TrackingEntry oldTrackingEntry = TrackingEntry.clone(trackingEntry);
        userTrackingUtility.trackSessionTimeUpTo180Seconds(realm, false, null);
        compareCounterDelta(trackingEntry, oldTrackingEntry);
    }

    @Test
    public void trackSessionTimeUpTo600Seconds() throws Exception {
        TrackingEntry trackingEntry = RealmDb.getTrackingEntryByKey(realm, RealmDb.RealmKeys.SESSION_TIME_UP_TO_600_SECONDS);
        TrackingEntry oldTrackingEntry = TrackingEntry.clone(trackingEntry);
        userTrackingUtility.trackSessionTimeUpTo600Seconds(realm, false, null);
        compareCounterDelta(trackingEntry, oldTrackingEntry);
    }

    @Test
    public void trackSessionTimeUpToInfinity() throws Exception {
        TrackingEntry trackingEntry = RealmDb.getTrackingEntryByKey(realm, RealmDb.RealmKeys.SESSION_TIME_INFINITY);
        TrackingEntry oldTrackingEntry = TrackingEntry.clone(trackingEntry);
        userTrackingUtility.trackSessionTimeUpToInfinity(realm, false, null);
        compareCounterDelta(trackingEntry, oldTrackingEntry);
    }

    @Test
    public void trackVideoCompletionUpTo25Percent() throws Exception {
        TrackingEntry trackingEntry = RealmDb.getTrackingEntryByKey(realm, RealmDb.RealmKeys.VIDEO_COMPLETION_UP_TO_25_PERCENT);
        TrackingEntry oldTrackingEntry = TrackingEntry.clone(trackingEntry);
        userTrackingUtility.trackVideoCompletionUpTo25Percent(realm, false, null);
        compareCounterDelta(trackingEntry, oldTrackingEntry);
    }

    @Test
    public void trackVideoCompletionUpTo50Percent() throws Exception {
        TrackingEntry trackingEntry = RealmDb.getTrackingEntryByKey(realm, RealmDb.RealmKeys.VIDEO_COMPLETION_UP_TO_50_PERCENT);
        TrackingEntry oldTrackingEntry = TrackingEntry.clone(trackingEntry);
        userTrackingUtility.trackVideoCompletionUpTo50Percent(realm, false, null);
        compareCounterDelta(trackingEntry, oldTrackingEntry);
    }

    @Test
    public void trackVideoCompletionUpTo75Percent() throws Exception {
        TrackingEntry trackingEntry = RealmDb.getTrackingEntryByKey(realm, RealmDb.RealmKeys.VIDEO_COMPLETION_UP_TO_75_PERCENT);
        TrackingEntry oldTrackingEntry = TrackingEntry.clone(trackingEntry);
        userTrackingUtility.trackVideoCompletionUpTo75Percent(realm, false, null);
        compareCounterDelta(trackingEntry, oldTrackingEntry);
    }

    @Test
    public void trackVideoCompletionUpTo100Percent() throws Exception {
        TrackingEntry trackingEntry = RealmDb.getTrackingEntryByKey(realm, RealmDb.RealmKeys.VIDEO_COMPLETION_UP_TO_100_PERCENT);
        TrackingEntry oldTrackingEntry = TrackingEntry.clone(trackingEntry);
        userTrackingUtility.trackVideoCompletionUpTo100Percent(realm, false, null);
        compareCounterDelta(trackingEntry, oldTrackingEntry);
    }

    @Test
    public void trackOfflineCount() throws Exception {
        TrackingEntry trackingEntry = RealmDb.getTrackingEntryByKey(realm, RealmDb.RealmKeys.OFFLINE_COUNT);
        TrackingEntry oldTrackingEntry = TrackingEntry.clone(trackingEntry);
        userTrackingUtility.trackOfflineCount(realm, false, null);
        compareCounterAndLastUpdatedDeltas(trackingEntry, oldTrackingEntry);
    }

    @Test
    public void trackBookmarkingCount() throws Exception {
        TrackingEntry trackingEntry = RealmDb.getTrackingEntryByKey(realm, RealmDb.RealmKeys.BOOKMARKING_COUNT);
        TrackingEntry oldTrackingEntry = TrackingEntry.clone(trackingEntry);
        userTrackingUtility.trackBookmarkingCount(realm, false, null);
        compareCounterAndLastUpdatedDeltas(trackingEntry, oldTrackingEntry);
    }

    @Test
    public void trackNightModeCount() throws Exception {
        TrackingEntry trackingEntry = RealmDb.getTrackingEntryByKey(realm, RealmDb.RealmKeys.NIGHT_MODE_COUNT);
        TrackingEntry oldTrackingEntry = TrackingEntry.clone(trackingEntry);
        userTrackingUtility.trackNightModeCount(realm, false, null);
        compareCounterAndLastUpdatedDeltas(trackingEntry, oldTrackingEntry);
    }

    @Test
    public void trackSettingsCount() throws Exception {
        TrackingEntry trackingEntry = RealmDb.getTrackingEntryByKey(realm, RealmDb.RealmKeys.SETTINGS_COUNT);
        TrackingEntry oldTrackingEntry = TrackingEntry.clone(trackingEntry);
        userTrackingUtility.trackSettingsCount(realm, false, null);
        compareCounterAndLastUpdatedDeltas(trackingEntry, oldTrackingEntry);
    }

    @Test
    public void trackTextSize() throws Exception {
        TrackingEntry trackingEntry = RealmDb.getTrackingEntryByKey(realm, RealmDb.RealmKeys.TEXT_SIZE_COUNT);
        TrackingEntry oldTrackingEntry = TrackingEntry.clone(trackingEntry);
        userTrackingUtility.trackTextSize(realm, false, null);
        compareCounterAndLastUpdatedDeltas(trackingEntry, oldTrackingEntry);
    }

    @Test
    public void trackLocalMarketSelectionCount() throws Exception {
        TrackingEntry trackingEntry = RealmDb.getTrackingEntryByKey(realm, RealmDb.RealmKeys.LOCAL_MARKET_SELECTION_COUNT);
        TrackingEntry oldTrackingEntry = TrackingEntry.clone(trackingEntry);
        userTrackingUtility.trackLocalMarketSelectionCount(realm, false, null);
        compareCounterAndLastUpdatedDeltas(trackingEntry, oldTrackingEntry);
    }

    @Test
    public void trackBreakingNewsNotificationsCount() throws Exception {
        TrackingEntry trackingEntry = RealmDb.getTrackingEntryByKey(realm, RealmDb.RealmKeys.BREAKING_NEWS_NOTIFICATIONS_COUNT);
        TrackingEntry oldTrackingEntry = TrackingEntry.clone(trackingEntry);
        userTrackingUtility.trackBreakingNewsNotificationsCount(realm, false, null);
        compareCounterAndLastUpdatedDeltas(trackingEntry, oldTrackingEntry);
    }

    @Test
    public void trackNewsNotificationsCount() throws Exception {
        TrackingEntry trackingEntry = RealmDb.getTrackingEntryByKey(realm, RealmDb.RealmKeys.NEWS_NOTIFICATIONS_COUNT);
        TrackingEntry oldTrackingEntry = TrackingEntry.clone(trackingEntry);
        userTrackingUtility.trackNewsNotificationsCount(realm, false, null);
        compareCounterAndLastUpdatedDeltas(trackingEntry, oldTrackingEntry);
    }

    @Test
    public void trackSportsNotificationsCount() throws Exception {
        TrackingEntry trackingEntry = RealmDb.getTrackingEntryByKey(realm, RealmDb.RealmKeys.SPORTS_NOTIFICATIONS_COUNT);
        TrackingEntry oldTrackingEntry = TrackingEntry.clone(trackingEntry);
        userTrackingUtility.trackSportsNotificationsCount(realm, false, null);
        compareCounterAndLastUpdatedDeltas(trackingEntry, oldTrackingEntry);
    }

    @Test
    public void trackLifeNotificationsCount() throws Exception {
        TrackingEntry trackingEntry = RealmDb.getTrackingEntryByKey(realm, RealmDb.RealmKeys.LIFE_NOTIFICATIONS_COUNT);
        TrackingEntry oldTrackingEntry = TrackingEntry.clone(trackingEntry);
        userTrackingUtility.trackLifeNotificationsCount(realm, false, null);
        compareCounterAndLastUpdatedDeltas(trackingEntry, oldTrackingEntry);
    }

    @Test
    public void trackMoneyNotificationsCount() throws Exception {
        TrackingEntry trackingEntry = RealmDb.getTrackingEntryByKey(realm, RealmDb.RealmKeys.MONEY_NOTIFICATIONS_COUNT);
        TrackingEntry oldTrackingEntry = TrackingEntry.clone(trackingEntry);
        userTrackingUtility.trackMoneyNotificationsCount(realm, false, null);
        compareCounterAndLastUpdatedDeltas(trackingEntry, oldTrackingEntry);
    }

    @Test
    public void trackTechNotificationsCount() throws Exception {
        TrackingEntry trackingEntry = RealmDb.getTrackingEntryByKey(realm, RealmDb.RealmKeys.TECH_NOTIFICATIONS_COUNT);
        TrackingEntry oldTrackingEntry = TrackingEntry.clone(trackingEntry);
        userTrackingUtility.trackTechNotificationsCount(realm, false, null);
        compareCounterAndLastUpdatedDeltas(trackingEntry, oldTrackingEntry);
    }
}
